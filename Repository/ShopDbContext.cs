﻿using System.Data.Entity;
using homework_18_6.Model;

namespace homework_18_6.Repository
{
    public sealed class ShopDbContext: DbContext
    {
        public ShopDbContext(): base("shop")
        {
        }
        
        public DbSet<Client> Clients { get; set; }
        
        public DbSet<Purchase> Purchases { get; set; }
    }
}