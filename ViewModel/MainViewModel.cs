﻿using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using homework_18_6.View;

namespace homework_18_6.ViewModel
{
    internal class MainViewModel : ViewModelBase
    {
        private Page _clientPage = new ClientPage();
        private Page _purchasePage = new PurchasePage();
        private Page _currentPage = new ClientPage();

        public Page CurrentPage
        {
            get => _currentPage;
            set => Set(ref _currentPage, value);
        }

        public ICommand OpenClientPage
        {
            get { return new RelayCommand(() => CurrentPage = _clientPage); }
        }

        public ICommand OpenPurchasePage
        {
            get { return new RelayCommand(() => CurrentPage = _purchasePage); }
        }
    }
}