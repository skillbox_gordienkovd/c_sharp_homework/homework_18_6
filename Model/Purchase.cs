﻿using System.ComponentModel.DataAnnotations.Schema;

namespace homework_18_6.Model
{
    public class Purchase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Email { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }
}