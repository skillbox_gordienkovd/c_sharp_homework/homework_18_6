﻿using System.ComponentModel.DataAnnotations.Schema;

namespace homework_18_6.Model
{
    public class Client
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Patronymic { get; set; }
        public string Lastname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}