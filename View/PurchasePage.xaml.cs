﻿using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;
using homework_18_6.Model;
using homework_18_6.Repository;

namespace homework_18_6.View
{
    public partial class PurchasePage
    {
        private readonly ShopDbContext _shopDbContext = new ShopDbContext();

        public PurchasePage()
        {
            InitializeComponent();

            _shopDbContext.Purchases.Load();

            PurchasesDataGrid.DataContext = _shopDbContext.Purchases.Local.ToBindingList();
        }

        private void PurchasesDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            _shopDbContext.SaveChanges();
        }

        private void MenuItemAddPurchaseClick(object sender, RoutedEventArgs e)
        {
            var purchase = new Purchase();
            var purchaseAddWindow = new PurchaseAddWindow(purchase);

            purchaseAddWindow.ShowDialog();

            if (purchaseAddWindow.DialogResult == null || !purchaseAddWindow.DialogResult.Value) return;

            _shopDbContext.Purchases.Add(purchase);
            _shopDbContext.SaveChanges();
        }

        private void MenuItemDeletePurchaseClick(object sender, RoutedEventArgs e)
        {
            var purchase = (Purchase) PurchasesDataGrid.SelectedItem;

            _shopDbContext.Purchases.Remove(purchase);
            _shopDbContext.SaveChanges();
        }
    }
}