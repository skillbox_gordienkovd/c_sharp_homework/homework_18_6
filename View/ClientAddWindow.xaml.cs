﻿using homework_18_6.Model;

namespace homework_18_6.View
{
    public partial class ClientAddWindow
    {
        private ClientAddWindow()
        {
            InitializeComponent();
        }

        public ClientAddWindow(Client client) : this()
        {
            Cancel.Click += delegate
            {
                DialogResult = false;
            };

            Add.Click += delegate
            {
                client.Firstname = Firstname.Text;
                client.Patronymic = Patronymic.Text;
                client.Lastname = Lastname.Text;
                client.PhoneNumber = PhoneNumber.Text;
                client.Email = Email.Text;

                DialogResult = true;
            };
        }
    }
}