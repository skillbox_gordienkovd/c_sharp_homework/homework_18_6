﻿using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;
using homework_18_6.Model;
using homework_18_6.Repository;

namespace homework_18_6.View
{
    public partial class ClientPage
    {
        private readonly ShopDbContext _shopDbContext = new ShopDbContext();
        public ClientPage()
        {
            InitializeComponent();

            _shopDbContext.Clients.Load();
            
            ClientsDataGrid.DataContext = _shopDbContext.Clients.Local.ToBindingList();
        }

        private void ClientsDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            _shopDbContext.SaveChanges();
        }
        
        private void MenuItemAddClientClick(object sender, RoutedEventArgs e)
        {
            var client = new Client();
            var addClientWindow = new ClientAddWindow(client);
            
            addClientWindow.ShowDialog();
            
            if (addClientWindow.DialogResult == null || !addClientWindow.DialogResult.Value) return;

            _shopDbContext.Clients.Add(client);
            _shopDbContext.SaveChanges();
        }
        
        private void MenuItemDeleteClientClick(object sender, RoutedEventArgs e)
        {
            var client = (Client)ClientsDataGrid.SelectedItem;

            _shopDbContext.Clients.Remove(client);
            _shopDbContext.SaveChanges();
        }
    }
}