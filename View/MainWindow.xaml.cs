﻿using System;
using System.Windows;

namespace homework_18_6.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void CloseProgram(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}