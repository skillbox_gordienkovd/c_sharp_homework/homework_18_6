﻿using homework_18_6.Model;

namespace homework_18_6.View
{
    public partial class PurchaseAddWindow
    {
        private PurchaseAddWindow()
        {
            InitializeComponent();
        }

        public PurchaseAddWindow(Purchase purchase) : this()
        {
            Cancel.Click += delegate
            {
                DialogResult = false;
            };

            Add.Click += delegate
            {
                purchase.Email = Email.Text;
                purchase.ProductCode = ProductCode.Text;
                purchase.ProductName = ProductName.Text;

                DialogResult = true;
            };
        }
    }
}